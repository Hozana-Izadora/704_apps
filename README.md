# Projeto Cadastro de Motoristas

Este projeto é um sistema de gerenciamento de informações de motoristas e veículos. A API foi desenvolvida em Laravel, enquanto o frontend foi desenvolvido em React. O sistema permite o cadastro, consulta, atualização e exclusão de registros de motoristas e veículos, facilitando o acompanhamento dos dados relacionados.

## Requisitos

- PHP (versão 7.4 ou superior)
- Composer (gerenciador de dependências do PHP)
- Node.js (versão 12 ou superior)
- Banco de dados PostgreSQL

## Instalação

1. Clone o repositório do projeto:

-`git clone https://gitlab.com/Hozana-Izadora/704_apps.git`


2. Acesse o diretório do projeto:

-`cd 704_apps`


3. Instale as dependências do backend:

-`composer install`


4. Configure as variáveis de ambiente:

Na pasta backend, renomeie o arquivo `.env.example` para `.env` e atualize as variáveis de acordo com a sua configuração, como as credenciais do banco de dados PostgreSQL.


5. Execute as migrações do banco de dados:

Certifique-se de executar a migração do banco de dados para a tabela de veículos primeiro, utilizando o comando acima, antes de executar as demais migrações. Isso garantirá a correta criação das tabelas no banco de dados.

Lembre-se de ter o PostgreSQL configurado corretamente antes de executar as migrações.

- `php artisan migrate --path=/database/migrations/2023_07_10_202931_create_vehicles_table.php`
- `php artisan migrate`


6. Inicie o servidor de desenvolvimento do backend:

`php artisan serve`
Certifique-se de iniciar o servidor do backend para que a API do projeto seja acessível. O comando -`php artisan serve` iniciará o servidor de desenvolvimento do Laravel e permitirá que você faça as requisições para as rotas da API.

6.1 Para cadastro de veículos foi criado uma factory. Inicialize o tinker do laravel e siga os passos:
- `php artisan tinker`
- `Vehicle::factory()->count(10)->create()`

6.2 Para fins de teste foi criado uma factory para popular as tabelas 'Drivers'. Inicialize o tinker do laravel e siga os passos:
- `Driver::factory()->create()`

6.3 Crie um usuário admin: 
- `User::factory()->create()`


7. Instale as dependências do frontend:

- `cd frontend`
- `npm install`



8. Inicie o servidor de desenvolvimento do frontend:
- `npm run dev`

9. Faça login com as segintes credenciais:
email: `user@email.com`
senha: `password`

Certifique-se de ter o PostgreSQL instalado e configurado corretamente antes de executar as migrações do banco de dados.

## Rotas da API

As seguintes rotas estão disponíveis na API para interação com os motoristas:

- `GET /drivers`: Recupera todos os motoristas cadastrados.
- `GET /drivers/{id}`: Recupera as informações do motorista correspondente ao ID.
- `POST /drivers`: Cria um novo registro de motorista e insere no banco de dados.
- `PATCH /drivers/{id}`: Atualiza um motorista existente correspondente ao ID.
- `DELETE /drivers/{id}`: Exclui o motorista com o ID fornecido.

As mesmas rotas também estão disponíveis para interação com os veículos:

- `GET /vehicles`: Recupera todos os veículos cadastrados.
- `GET /vehicles/{id}`: Recupera as informações do veículo correspondente ao ID.
- `POST /vehicles`: Cria um novo registro de veículo e insere no banco de dados.
- `PATCH /vehicles/{id}`: Atualiza um veículo existente correspondente ao ID.
- `DELETE /vehicles/{id}`: Exclui o veículo com o ID fornecido.

## Model de Drivers

O Model de Drivers representa as informações relacionadas aos motoristas no projeto. Ele possui os seguintes campos:

- `vehicle_id`: ID do veículo associado ao motorista.
- `name`: Nome do motorista.
- `age`: Idade do motorista.
- `cpf`: CPF do motorista.
- `cnh`: CNH do motorista.
- `email`: E-mail do motorista.
- `phone`: Telefone do motorista.
- `address`: Endereço do motorista.
- `address_number`: Número do endereço do motorista.
- `address_state`: Estado do endereço do motorista.
- `address_city`: Cidade do endereço do motorista.
- `address_complement`: Complemento do endereço do motorista.
- `zipcode`: CEP do endereço do motorista.

## Model de Vehicles

O Model de Vehicles representa as informações relacionadas aos veículos no projeto. Ele possui os seguintes campos:

- `model`: Modelo do veículo.
- `fuel_type`: Tipo de combustível utilizado pelo veículo.
- `brand`: Marca do veículo.
- `fabrication_year`: Ano de fabricação do veículo.
- `plate`: Placa do veículo.
- `color`: Cor do veículo.

Certifique-se de configurar corretamente todas as dependências e variáveis de ambiente antes de executar o projeto.

Para mais informações e contato, consulte o arquivo LICENSE e entre em contato através do e-mail: [izadoraferreir@gmail.com](mailto:izadoraferreir@gmail.com).




