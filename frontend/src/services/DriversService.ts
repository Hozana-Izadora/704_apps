import { IDrivers } from "../interfaces/IDrivers";
import driverFetch from "./api";

const getAll = ()=>
    driverFetch.get<IDrivers[]>('drivers')
    .then((response) => {
        return response.data.data;
    });

export const DriversService = {
    getAll
};