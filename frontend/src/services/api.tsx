import axios from "axios"

const token = localStorage.getItem('authToken')

const driverFetch = axios.create({
    baseURL: "http://127.0.0.1:8000",
    headers: {
        "Access-Control-Allow-Origin": "*",
        "Authorization": `Bearer ${token}`,
    }
});


export default driverFetch;