import React, { useContext, useMemo } from "react";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import './Home.css'
import { AuthContext } from "../contexts/Auth/AuthContext";
import { useDrivers } from "../hooks/useDrivers";

const Home = () => {
    const auth = useContext(AuthContext);
    const {drivers, getAll} = useDrivers();

    const [search, setSearch] = useState('')       
    
    useEffect(() => {
        getAll();
    }, [getAll]) 
 
    const filtersDrivers = useMemo(()=>{
        const searchInput = search.toLowerCase()
        // console.log(drivers)
        return drivers
        .filter((driver) => {
            if(driver.name.toLowerCase().includes(searchInput)){return drivers}
            else if (driver.vehicle.model.toLowerCase().includes(searchInput)){
                return drivers
            }
            else if (driver.cpf.includes(searchInput)){
                return drivers
            }
            else if (driver.vehicle.plate.toLowerCase().includes(searchInput)){
                return drivers
            }
        })
    },[search])  

  return (
    
    <div className="home card">
        <div className="filter">
          <input  value={search} type="text" name="filter" id="filter"  placeholder="Find the driver here" onChange={(e)=>setSearch(e.target.value)}/>
        </div>
    <h1>All Drivers</h1>
    {filtersDrivers.length == 0 ? (<p>Loading...</p>): (
        filtersDrivers.map((driver)=>(
            <div className="driver" key={driver.identify}>
                <h2>{driver.identify} - {driver.name}</h2>
                <p>Vehicle: {driver.vehicle.model}</p>
                <Link to={`/show/${driver.identify}`} className="btn">
                    More Details
                </Link>
            </div>
        ))
    )}
    
    </div>
  );

};

export default Home