import { useEffect, useState } from 'react'
import driverFetch from '../../services/api'
import { Navigate, useParams } from 'react-router-dom'
import {Link, Route, useNavigation} from 'react-router-dom';
import '../Home.css'
import './Driver.css'
import Swal from 'sweetalert2'

const redirect = (url:string, asLink = true) =>
  asLink ? (window.location.href = url) : window.location.replace(url);

const DriverShow = () => {
    const [driver, setDriver] = useState([]);
    const {id} = useParams();
    const getDriver = async () => {
        
        await driverFetch.get(`/drivers/${id}`)
        .then((response)=> setDriver(response.data.data))           
        console.log(driver)

    }
    useEffect(() => {
        getDriver();
    }, [driver])
    function showAlert(){
        Swal.fire({
            icon: 'question',
            title: 'Do you want to delete this register?',
            showCancelButton: true,
            confirmButtonText: 'Delete',
            confirmButtonColor: '#a83238',
            }).then((result) => {
            /* Read more about isConfirmed, isDenied below */
            if (result.isConfirmed) {
                driverFetch.delete(`/drivers/${id}`).
                then(function (resp) {
                    console.log(resp)
                    Swal.fire('Register deleted!', '', 'success')
                    redirect('/')
                    
                }).catch(function(error){
                    Swal.fire('Register not deleted!', '', 'error')
                })
            } 
        })  

    }
  return (
    <div className="home card">
        <Link to={`/edit/${id}`} className='new-btn'>Edit Driver</Link>
        <button className='delete-btn' onClick={showAlert}>Delete Driver</button>

        <h1>DRIVER'S INFORMATIONS</h1>

        <h2>Identify: {driver.identify}</h2>
        <h2>Name: {driver.name}</h2>
        <h3>CPF: {driver.cpf}</h3>
        <h3>CNH: {driver.cnh}</h3>
        <h3>Email: {driver.email}</h3>
        <h3>Phone: {driver.phone}</h3>

        <hr />
        <h1>ADRESS'S INFORMATIONS</h1>
        <h2>Adress: {driver.adress}, {driver.adress_number}</h2>
        <h3>City/State: {driver.adress_state}, {driver.adress_city}</h3>
        <h3>Complement: {driver.adress_complement}, Zipcode: {driver.zipcode}</h3>

        <hr />
        <h1>VEHICLE'S INFORMATIONS</h1>
        
{/*         
            <h2>Identify: {driver.vehicle_id}</h2>
            <h2>Brand: {driver.vehicle.brand}</h2> 
            <h3>Model: {driver.vehicle.model}</h3>
            <h3>Plate: {driver.vehicle.plate} </h3>
            <h3>Fabrication Year: {driver.vehicle.fabrication_year} </h3>
            <h3>Color: {driver.vehicle.color} </h3> 
         */}


    </div>
  )
}

export default DriverShow