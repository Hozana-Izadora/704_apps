import './Driver.css';
import driverFetch from '../../services/api';
import { useState, useEffect } from 'react';
import { json, useNavigate, useParams } from 'react-router-dom';
import { useVehicles } from '../../hooks/useVehicles';
import Swal from 'sweetalert2';

const DriverEdit = () => {
    const {id} = useParams();
    const navigate = useNavigate()
    const {vehicles} = useVehicles();
  
    const [name, setName] = useState();
    const [vehicle_id, setVehicle] = useState();
    const [age, setAge] = useState();
    const [cpf, setCpf] = useState();
    const [cnh, setCnh] = useState();
    const [email, setEmail] = useState();
    const [phone, setPhone] = useState();
    const [adress, setAdress] = useState();
    const [adress_number, setAdressNB] = useState();
    const [adress_state, setAdressST] = useState();
    const [adress_city, setAdressCT] = useState();
    const [adress_complement, setAdressCP] = useState();
    const [zipcode, setAdressZip] = useState();

  
   const createPost = async(e) =>{
    e.preventDefault();
    await driverFetch.patch(`/drivers/${id}`, {
      vehicle_id,
      name,
      age,
      cpf,
      cnh,
      email,
      phone,
      adress,
      adress_number,
      adress_state,
      adress_city,
      adress_complement,
      zipcode
    }).then(function (resp) {
      Swal.fire({
        icon: 'success',
        title: "Success",
        text: 'Register edited!',
        showConfirmButton: false,
        timer: 1500,
        position: 'top-end'
      })
    })
    .catch(function (error) {
      const erro = error.response.data.errors    
      Swal.fire({
        icon: 'error',
        title: `Error: ${error.response.status} - ${error.response.data.message}`,
        html: JSON.stringify(erro,null,'\t'), 
      })
    });
  
    navigate(`/show/${id}`);
   }


    return (
      <div className='new-driver'>
        <h2>Register Driver</h2>
        <form onSubmit={(e) => createPost(e)}>
          <div className="form-control">
            <label htmlFor="title">Driver's Name:</label>
            <input type="text" name="name" id="name"  placeholder='Full Name' onChange={(e)=>setName(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="vehicle_id">Driver's Vehicle:</label>
            <select name="vehicle_id" id="vehicle_id" onChange={(e)=>setVehicle(e.target.value)}>
              <option value=""></option>
              {vehicles.map(vehicle => <option key={vehicle.identify} value={vehicle.identify}>{vehicle.brand} {vehicle.model}</option>)}
            </select>
          </div>
          <div className="form-control">
            <label htmlFor="age">Driver's Age:</label>
            <input type="text" name="age" id="age"  placeholder="Inform the driver's age" onChange={(e)=>setAge(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="cpf">Driver's CPF:</label>
            <input type="text" name="cpf" id="cpf"  placeholder="Inform the driver's CPF" onChange={(e)=>setCpf(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="cnh">Driver's CNH:</label>
            <input type="text" name="cnh" id="cnh"  placeholder="Inform the driver's CNH" onChange={(e)=>setCnh(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="email">Driver's Email:</label>
            <input type="email" name="email" id="email"  placeholder="Inform the driver's Email" onChange={(e)=>setEmail(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="phone">Driver's Phone:</label>
            <input type="phone" name="phone" id="phone"  placeholder="Inform the driver's Phone" onChange={(e)=>setPhone(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="adress">Driver's Adress:</label>
            <input type="text" name="adress" id="adress"  placeholder="Inform the driver's Adress" onChange={(e)=>setAdress(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="adress_number">Driver's Adress Number:</label>
            <input type="text" name="adress_number" id="adress_number"  placeholder="Inform the driver's Adress Number" onChange={(e)=>setAdressNB(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="adress_state">Driver's Adress State:</label>
            <input type="text" name="adress_state" id="adress_state"  placeholder="Inform the driver's Adress State" onChange={(e)=>setAdressST(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="adress_city">Driver's Adress City:</label>
            <input type="text" name="adress_city" id="adress_city"  placeholder="Inform the driver's Adress City" onChange={(e)=>setAdressCT(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="adress_complement">Driver's Adress Complement:</label>
            <input type="text" name="adress_complement" id="adress_complement"  placeholder="Inform the driver's Adress Complement" onChange={(e)=>setAdressCP(e.target.value)}/>
          </div>
          <div className="form-control">
            <label htmlFor="zipcode">Driver's Adress Zipcode:</label>
            <input type="text" name="zipcode" id="zipcode"  placeholder="Inform the driver's Zipcode" onChange={(e)=>setAdressZip(e.target.value)}/>
          </div>
          <input type="submit" value="Submit" className='btn' />
        </form>
      </div>
    )
}

export default DriverEdit