import React, { useContext, useState } from 'react'
import '../Driver/Driver.css'
import { AuthContext } from '../../contexts/Auth/AuthContext';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

const Login = () => {
    const auth = useContext(AuthContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const navigate  = useNavigate();
    const handleEmailInput = (e: React.ChangeEvent<HTMLInputElement> ) =>{
        setEmail(e.target.value)
    }

    const handlePasswordInput = (e: React.ChangeEvent<HTMLInputElement>) =>{
        setPassword(e.target.value)
    }
    const handleLogin = async ()=> {
        if(email && password){
            const isLogged = await auth.signin(email,password);
            console.log(isLogged)
            if(isLogged){
                navigate('/')
            }else{
                Swal.fire('Error', "You're not authorized", 'error')

            }
        }
    }

  return (
    <div className='form-control align-center'>
        <h2>Login</h2>
        <input type="text"  value={email}onChange={handleEmailInput} placeholder='Enter with your email'/>
        <input type="password" value={password} onChange={handlePasswordInput} placeholder='Enter with your password'/>
       <br />
        <button type="submit" onClick={handleLogin} className='enter-btn'> Entrar</button>
    </div>
  )
}

export default Login