import { useEffect, useState } from "react"
import driverFetch from "../services/api"

export const useVehicles = () =>{

    interface IVehicle{
        identify: number;
        model: string;
        fuel_type:string;
        brand:string;
        fabrication_year:number;
        plate:string;
        color:string;
    }
    const [vehicles, setVehicles] = useState<IVehicle[]>([])

    useEffect(()=>{
        driverFetch('vehicles')
        .then((response) => response.data.data)
        .then((data)=> setVehicles(data))
    }, []);

    return {
        vehicles
    }
}