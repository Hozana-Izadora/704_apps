import { useCallback, useState } from "react"
import { DriversService } from "../services/DriversService"
import { IDrivers } from "../interfaces/IDrivers";

export const useDrivers = () => {
    const [drivers, setDrivers] = useState<IDrivers[]>([]);
    const getAll = useCallback(async () => {
        const data = await DriversService.getAll();
            setDrivers(data)
        
    }, []);
    return {
        drivers,
        getAll
    }
}
