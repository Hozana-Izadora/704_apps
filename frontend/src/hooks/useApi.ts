import driverFetch from "../services/api";

export const useApi = ()=> ({
    validateToken: async (token: string)=>{
        return {
            user: {name:'User', email: 'user@email.com'}
        };
    },
    signin: async (email:string, password:string) => {

        const response = await driverFetch.post('/login',{email,password})
        return response.data;
    },

});