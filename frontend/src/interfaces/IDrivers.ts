export interface IDrivers {
identify: number
name: string
email: string
age: number
cpf: string
cnh: string
phone: string
adress: string
adress_number: string
adress_state: string
adress_city: string
adress_complement: string
zipcode: string
vehicle_id: string
vehicle: Vehicle
}

export interface Vehicle {
identify: number
model: string
fuel_type: string
brand: string
fabrication_year: string
plate: string
color: string
created: string
}
  