import React from 'react'
import { Link } from 'react-router-dom'
import './Navbar.css'

const Navbar = () => {
  return (
    <nav className='navbar'>
      <h2><Link to={'/'}><img  width="100px" src="./logo.webp"></img></Link> </h2>
        <ul>
            <li><Link to={'/'}>Home</Link></li>
            <li><Link to={'/create'} className='new-btn'>New Driver</Link></li>
        </ul>
    </nav>
  )
}

export default Navbar