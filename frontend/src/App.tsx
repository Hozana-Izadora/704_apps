import { Outlet, Route, Routes } from 'react-router-dom';
import Navbar from './components/Navbar';
import './App.css'
import Home from './pages/Home';
import DriverCreate from './pages/Driver/DriverCreate';
import DriverShow from './pages/Driver/DriverShow';
import DriverEdit from './pages/Driver/DriverEdit';
import { RequireAuth } from './contexts/Auth/RequireAuth';
import Login from './pages/Login/Login';

function App() {
  return (
    <div className='App'>
      <Navbar />
      <div className='container'>
        <Routes>
          <Route path='/login' element={<Login />}></Route>
          <Route path='/' element={<RequireAuth><Home /></RequireAuth>}></Route>
          <Route path='/create' element={<RequireAuth><DriverCreate/></RequireAuth>}></Route>
          <Route path='/show/:id' element={<RequireAuth><DriverShow /></RequireAuth>}></Route>
          <Route path='/edit/:id' element={<RequireAuth><DriverEdit /></RequireAuth>}></Route>
        </Routes>
      </div>
    </div>
  )
}

export default App
