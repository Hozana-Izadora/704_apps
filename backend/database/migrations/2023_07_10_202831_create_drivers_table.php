<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * ID: Identificador único do motorista (chave primária)
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('vehicle_id')->constrained('vehicles')->onDelete('cascade');;
            $table->char('name',250);
            $table->integer('age');
            $table->char('cpf',20);
            $table->char('cnh',20);
            $table->char('email',100);
            $table->char('phone',20);
            $table->char('adress',250);
            $table->char('adress_number',20);
            $table->char('adress_state',100);
            $table->char('adress_city',100);
            $table->char('adress_complement',20);
            $table->char('zipcode',20);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');        
    }
}
