<?php

namespace Database\Factories;
use Faker;
use Faker\Provider\pt_BR\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

class DriverFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'vehicle_id'=> $this->faker->unique()->numberBetween(1,10),
            'name' => $this->faker->name(),
            'age' => $this->faker->numberBetween(18,80),
            'cpf' => $this->faker->numerify('###.###.###-##'),
            'cnh' => $this->faker->numerify('###########'),
            'email'=> $this->faker->email(),
            'phone' => $this->faker->phoneNumber(),
            'adress' => $this->faker->streetAddress(),
            'adress_number' => $this->faker->buildingNumber(),
            'adress_state' => $this->faker->state(),
            'adress_city' => $this->faker->city(),
            'adress_complement' => $this->faker->secondaryAddress() ,
            'zipcode'=>$this->faker->postcode(),
        ];
    }
}
