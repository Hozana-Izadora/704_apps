<?php

namespace Database\Factories;
use Faker\Provider\Fakecar;
use Faker\Core\Color;
use Illuminate\Database\Eloquent\Factories\Factory;

class VehicleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
  
    public function definition()
    {
        $this->faker->addProvider(new Fakecar($this->faker));
        $vehicle = $this->faker->vehicleArray();

        return [
            'model'           => $vehicle['model'],
            'brand'           => $vehicle['brand'],
            'fabrication_year'  => $this->faker->biasedNumberBetween(1990, date('Y'), 'sqrt'),
            'plate'  => $this->faker->vehicleRegistration,
            'color'  =>$this->faker->colorName(),
            'fuel_type'       => $this->faker->vehicleFuelType,
        ];
    }
}
