<?php

use App\Http\Controllers\Api\DriversController as ApiDriversController;
use App\Http\Controllers\Api\VehicleController;
use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//DRIVERS
Route::apiResource('/drivers', ApiDriversController::class);

//VEHICLES
Route::apiResource('/vehicles', VehicleController::class);

//AUTENTICATION

Route::post('/login',[AuthController::class,'login']);


Route::get('/',function(){
    return response()->json([
        'success'=>true
    ]);
});
 