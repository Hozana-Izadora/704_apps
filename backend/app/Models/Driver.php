<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    use HasFactory;
    protected $fillable = [
        'vehicle_id',
        'name',
        'age',
        'cpf',
        'cnh',
        'email',
        'phone',
        'adress',
        'adress_number',
        'adress_state',
        'adress_city',
        'adress_complement',
        'zipcode'

    ];
    public function vehicles()
    {
        return $this->belongsTo(Vehicle::class);
    }

} 
