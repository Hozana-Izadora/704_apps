<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;
    protected $fillable = [
        'model',
        'fuel_type',
        'brand',
        'fabrication_year',
        'plate',
        'color'
    ];

    public function drivers()
    {
        return $this->hasMany(Driver::class);
    }
}
