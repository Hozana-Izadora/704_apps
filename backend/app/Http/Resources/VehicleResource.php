<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class VehicleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'identify'=> $this->id,
            'model' => rtrim($this->model),
            'fuel_type' => rtrim($this->fuel_type),
            'brand'=> rtrim($this->brand),
            'fabrication_year' => rtrim($this->fabrication_year),
            'plate' => rtrim($this->plate),
            'color'  => rtrim($this->color),
            'created' => Carbon::make($this->created_at)->format('d/m/Y')
        ];
        // return parent::toArray($request);
    }
}
