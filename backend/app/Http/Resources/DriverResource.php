<?php

namespace App\Http\Resources;

use App\Models\Vehicle;
use Illuminate\Http\Resources\Json\JsonResource;

class DriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $vehicle = Vehicle::find($this->vehicle_id);
        return[
            'identify' => $this->id,
            'name' => rtrim($this->name),
            'email'=> rtrim($this->email),
            'age'  => $this->age,
            'cpf'  => rtrim($this->cpf),
            'cnh'  => rtrim($this->cnh),
            'phone' => rtrim($this->phone),
            'adress' => rtrim($this->adress),
            'adress_number' => rtrim($this->adress_number),
            'adress_state' => rtrim($this->adress_state),
            'adress_city' => rtrim($this->adress_city),
            'adress_complement' => rtrim($this->adress_complement),
            'zipcode' => rtrim($this->zipcode),
            'vehicle_id' => rtrim($this->vehicle_id),
            'vehicle'=> new VehicleResource($vehicle),
        ];
    }
}
