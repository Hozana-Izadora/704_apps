<?php

namespace App\Http\Requests;

// use Illuminate\Contracts\Validation\Rule;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule as ValidationRule;

class StoreUpdateDriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => [
                'required',
                'min:3',
                'max:100'
            ],
            'email' => [
                'required',
                'email',
                'max:255',
                'unique:drivers'
            ],
            'cpf' => [
                'required',
                'max:15',
                'min:11',
                'unique:drivers'
            ],
            'vehicle_id' => [
                'required',
                'unique:drivers'
            ]
        ];

        if($this->method() === 'PATCH'){
            $rules['name'] = [
                'nullable',
                'min:3',
                'max:100',
            ];
            $rules['email'] = [
                'email',
                'max:255',
                ValidationRule::unique('drivers')->ignore($this->id),
            ];
            $rules['cpf'] = [
                'max:15',
                'min:11',
                ValidationRule::unique('drivers')->ignore($this->id),
            ];
            $rules['vehicle_id'] = [
                ValidationRule::unique('drivers')->ignore($this->id),
            ];           

        }
        // if($this->method() === 'POST'){
        //     $rules['name'] = [
        //         'nullable',
        //         'min:3',
        //         'max:100',
        //     ];
        //     $rules['email'] = [
        //         'required',
        //         'email',
        //         'max:255',
        //         ValidationRule::unique('drivers')->ignore($this->id),
        //     ];
        //     $rules['cpf'] = [
        //         'required',
        //         'max:15',
        //         'min:11',
        //         ValidationRule::unique('drivers')->ignore($this->id),
        //     ];
        //     $rules['vehicle_id'] = [
        //         'required',
        //         ValidationRule::unique('drivers')->ignore($this->id),
        //     ];           

        // }

        return $rules;
    }
}
