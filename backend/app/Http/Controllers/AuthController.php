<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AuthController extends Controller
{
    
    public function login(Request $request){
        if(Auth::attempt($request->only('email','password'))){
            $token = $request->user()->createToken('invoice')->plainTextToken;
            return response()->json([
                'message'=> 'Authorized',
                'token'=>$token
            ], 202, ['token'=>$token]);

        };
        return response()->json(['message'=> 'Forbiden'], 403);
                       
    }

}
